import React from 'react';
import { Row, Col, Button } from 'antd';
import { LogoutOutlined, ReloadOutlined } from '@ant-design/icons';
import { useAsync } from "react-async-hook";

import { getBalance, getAddress, purge } from '../asgardex-bitcoin';

const Controls = ({ setPhrase }) => {
  const balance = useAsync(getBalance, []);
  const address = getAddress();

  const logout = () => {
    purge();
    setPhrase(false);
  };

  return (
    <div>
      <Row>
        <Col xs={12}>
          <h2>Your account:</h2>
        </Col>
        <Col xs={12} style={{ textAlign: 'right' }}>
          <Button type="text" onClick={logout} icon={<LogoutOutlined />}>
            Logout
          </Button>
        </Col>
      </Row>
      <p>Address: <b>{address}</b></p>
      <p>
        Balance: {balance.loading ? '' : <b>{balance.result}</b>}
        <Button type="text" onClick={() => balance.execute()} icon={<ReloadOutlined />}>
          Refresh balance
        </Button>
      </p>
    </div>
  );
};

export default Controls;