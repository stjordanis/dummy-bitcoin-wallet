import React from 'react';
import { Table } from 'antd';
import { useAsync } from 'react-async-hook';

import { getTransactions } from '../asgardex-bitcoin';

const TransactionsTable = () => {
  const { loading: transactionsLoading, result: transactions } = useAsync(getTransactions, []);

  return (
    <div>
      <Table
        loading={transactionsLoading}
        dataSource={transactions}
        rowKey={record => record.txid}
      >
        <Table.Column
          title="TX ID"
          dataIndex="txid"
        />
        <Table.Column
          dataIndex="txid"
          render={(text) => (
            <a
              href={`https://blockstream.info/testnet/tx/${text}`}
              target="_blank"
              rel="noopener noreferrer"
            >
              View
            </a>
          )}
        />
      </Table>
    </div>
  );
};

export default TransactionsTable;